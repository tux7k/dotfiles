VOL2=$(pw-volume status | jq .percentage)

if (($VOL2 == 0)); then
	VOL="🔇 $VOL2"
elif (($VOL2 < 25)); then
	VOL="🔈 $VOL2"
elif (($VOL2 < 50)); then
	VOL="🔉 $VOL2"
elif (($VOL2 < 100)); then
	VOL="🔊 $VOL2"
fi

echo ${VOL}%